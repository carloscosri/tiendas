<?php
require_once 'lib/Controller.php';

class Index extends Controller
{

    public function __construct()
    {
        parent::__construct('Index');
    }
    
    public function __toString(){
        return 'INDEX';
    }
    public function method()
    {
        $this->view->setMethod("METHOD");
        $this->view->render();
    }
    
    public function index()
    {
        $this->view->render();

    }

}