<?php

require_once 'lib/View.php';

class OrderView extends View
{
    function __construct()
    {
        parent::__construct();
//        echo 'En la vista Index<br>';
    }

    public function render($pedidos, $plantilla='order.tpl')
    {
        $js[] = 'ajaxProduct.js';
        $this->smarty->assign('js', $js);
        $this->smarty->assign('pedidosHechos', $pedidos);
        $this->smarty->assign('listaPedido', $_SESSION['listaPedido']);
        $this->smarty->display($plantilla);
    }
}