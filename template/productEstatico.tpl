{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <h2>{$language->translate('product_list')}</h2>
    
    <p><a href="{$url}{$lang}/product/add">{$language->translate('new_product')}</a></p>
    <table>
        <tr>
            <th>Id</th>
            <th>{$language->translate('code')}</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('price')}</th>
            <th>{$language->translate('existence')}</th>
            <th>{$language->translate('operations')}</th>
        </tr>
        
        {foreach $rows as $row}
            <tr>
                <td>{$row.id}</td>
                <td>{$row.codigo}</td>
                <td>{$row.nombre}</td>
                <td>{$row.precio}</td>
                <td>{$row.existencia}</td>
                <td>
                    <a href="{$url}{$lang}/product/edit/{$row.id}" >editar</a>
                    <a href="{$url}{$lang}/product/delete/{$row.id}" >borrar</a>                    
                </td>
            </tr>
        {/foreach}
    </table>
    
    <p><a href="{$url}{$lang}/product/add">{$language->translate('new_product')}</a></p>

 </div>
{include file="template/footer.tpl" title="footer"}