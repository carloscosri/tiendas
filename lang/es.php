<?php

$es = array(
    'operations' => 'Operaciones',
    'name' => 'Nombre',
    'user_list' => 'Lista de usuarios',
    'edit' => 'Editar',
    'delete' => 'Borrar',
    'new_user' => 'Nuevo usuario',
    'index' => 'Inicio',
    'user'=> 'Usuarios',
    'error_password' => 'La contraseña debe tener entre 6 y 20 caracteres',
    //
    'product'=> 'Productos',
    'product_list' => 'Lista de productos',
    'new_product' => 'Nuevo producto',
    'login' => 'Login',
    'code'=> 'Codigo',
    'price'=> 'Precio',
    'existence'=> 'Existencia',
    //controles select
    'select_one' => 'seleccionar uno  ------------',
    'order' => 'Pedido',
    'register' => 'Registro',
    'filter' => 'Filtro',
);