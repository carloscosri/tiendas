<?php

$en = array(
    'operations' => 'Operations',
    'name' => 'Name',
    'user_list' => 'User list',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'new_user' => 'New user',
    'index' => 'Index',
    'user'=> 'User',
    'error_password' => 'The password must be between 6 and 20 characters',
    //
    'product'=> 'Product',
    'product_list' => 'Product List',
    'new_product' => 'New product',
    'login' => 'Login',
    'code'=> 'Code',
    'price'=> 'Price',
    'existence'=> 'Existence',
    //controles select
    'select_one' => 'select one   ------------',
    'order' => 'Order',
    'register' => 'Register',
    'filter' => 'Filter',
);